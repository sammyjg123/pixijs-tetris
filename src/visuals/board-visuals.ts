import { cloneDeep } from "lodash";
import { Texture, Sprite, Container, Graphics, graphicsUtils, Matrix } from "pixi.js";
import { Game } from "src/game";
import { gradient } from "src/utils/colour-utils";
import { Board } from "../logic/board";
import { Shape } from "../logic/shape";
import { GameScreen } from "./game-screen";

export class BoardVisuals {
    
    private borderWidth: number;
    private blockTexture: Texture;
    private defaultBlockWidth: number;
    private defaultBlockHeight: number;
    private shapeColours: number[];

    readonly boardObjects: Container;
    private boardFrame: Container;
    private boardMask: Container;
    private currentDrawnShape: Container;
    private currentDrawnBoard: Container;

    public boardPositionX: number;
    public boardPositionY: number;

    constructor() {
        this.boardPositionX = 0;
        this.boardPositionY = 0;

        this.boardObjects = new Container();
        this.boardFrame = new Container();
        this.boardMask = new Container();
        this.currentDrawnShape = new Container();
        this.currentDrawnShape.mask = this.boardMask;
        this.currentDrawnBoard = new Container();
        this.defaultBlockHeight = 35;
        this.defaultBlockWidth = 35;
        this.borderWidth = 5;

        this.blockTexture = Texture.from("/textures/TetrisBlock_Half.png");
        this.shapeColours = [
            0x0341AE,
            0x72CB3B,
            0xFFD500,
            0x33FFB7,
            0x5EFF33,
            0xC9FF33,
            0xFF8A33
        ];

        this.boardObjects.addChild(this.boardFrame, this.boardMask, this.currentDrawnBoard, this.currentDrawnShape);
        this.initBoardVisuals();

        GameScreen.instance.screenObjects.addChild(this.boardObjects);
    }

    public updateCurrentBoard(board: Board, rowsRemoved?: number[]): void {;
        if (rowsRemoved != undefined) {
            rowsRemoved.forEach(rowIndex => {
                // this.animateRow(rowIndex);
            });
        }
        let newBoard = new Container();
        let rowContainer;
        for (let y = 0; y < board.getBoard().length; y++) {
            rowContainer = new Container();
            for (let x = 0; x < board.getBoard()[y].length; x++) {
                if (board.getBoard()[y][x] > 0) {
                    const block = new Sprite(this.blockTexture);
                    block.tint = this.shapeColours[board.getBoard()[y][x] - 1];
                    block.width = this.defaultBlockWidth;
                    block.height = this.defaultBlockHeight;
                    block.x = x * block.width;
                    block.y = y * block.height;
                    rowContainer.addChild(block);
                }
            }
            newBoard.addChild(rowContainer);
        }
        this.currentDrawnBoard.removeChildren();
        this.currentDrawnBoard.addChild(...newBoard.removeChildren());
        board.isDirty = false;
    }

    public updateCurrentShape(shape: Shape, softHeight: number): void {
        if (shape.isDirty) {
            const newContainer = new Container();
            newContainer.x = this.currentDrawnShape.x;
            newContainer.y = this.currentDrawnShape.y;
            const iterationCount = this.currentDrawnShape.children.length;

            for (let i = 0; i < iterationCount; i++) {
                this.currentDrawnShape.removeChildAt(0);
            }
            if (!shape.didRotate) {
                this.currentDrawnShape.x = 0;
                this.currentDrawnShape.y = this.defaultBlockHeight * -softHeight;
            }

            for (let y = 0; y < shape.grid.length; y++) {
                for (let x = 0; x < shape.grid[y].length; x++) {
                    if (shape.grid[y][x] > 0) {
                        const block = new Sprite(this.blockTexture);
                        block.tint = this.shapeColours[shape.grid[y][x] - 1];
                        block.width = this.defaultBlockWidth;
                        block.height = this.defaultBlockHeight;
                        block.x = x * block.width;
                        block.y = y * block.height;
                        this.currentDrawnShape.addChild(block);
                    }
                }
            }
            shape.isDirty = false;
        } else {
            this.currentDrawnShape.x = shape.locationX * this.defaultBlockWidth;
            this.currentDrawnShape.y = shape.locationY * this.defaultBlockHeight;
        }
    }

    private initBoardVisuals() {
        const pixelWidth = this.defaultBlockWidth * Game.boardWidth;
        const pixelHeight = this.defaultBlockHeight * Game.boardHeight;

        let boardGraphics = new Graphics();
        boardGraphics.beginTextureFill({texture: gradient(this.shapeColours), matrix: new Matrix(pixelWidth / 100, 0, 0, pixelHeight / 100), alpha: 0.5});
        boardGraphics.lineStyle(0, 0x6C6C6C, 1);
        boardGraphics.drawRect(0, 0, pixelWidth, pixelHeight);
        boardGraphics.endFill();
        boardGraphics.alpha = 0.5;
        this.boardFrame.addChild(cloneDeep(boardGraphics));

        boardGraphics = new Graphics();
        boardGraphics.beginFill(0xFFFFFF);
        boardGraphics.drawRect(0, 0, pixelWidth, pixelHeight);
        this.boardMask.addChild(boardGraphics);

        this.boardPositionX = (GameScreen.instance.canvasWidth / 2) - (pixelWidth / 2);
        this.boardPositionY = (GameScreen.instance.canvasHeight / 2)  - (pixelHeight / 2);
        this.boardObjects.position.y = this.boardPositionY;
        this.boardObjects.position.x = this.boardPositionX;
    }

    

}