export class ShapeDefinition {

    readonly shapeName: string;
    readonly defaultGrid: number[][];
    readonly rotationOffset: number;
    readonly definitionID: number;

    constructor(shapeName: string, id: number, rotationOffset: number, grid: number[][]) {
        this.shapeName = shapeName;
        this.definitionID = id;
        this.rotationOffset = rotationOffset;
        this.defaultGrid = grid.map(y => y.map(x => x * id));
    }

}