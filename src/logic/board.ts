export class Board {

    private board: number[][];
    private softHeight: number;
    public isDirty: boolean;
    public hasExceededHeight;

    constructor(height: number, width: number, softHeight: number) {
        this.board = Array(width).fill(0).map(x => Array(height).fill(0));
        this.softHeight = this.board.length + softHeight;
        this.isDirty = false;
        this.hasExceededHeight = false;
    }

    public tryMove(shape: number[][], x: number, y: number): boolean {
        if (!this.isInBounds(shape, x, y)) return false;
        if (this.isCollision(shape, x, y)) return false;

        return true;
    }

    public setPosition(shape: number[][], shapeX: number, shapeY: number): number[] {
        for (let y = 0; y < shape.length; y++) {
            for (let x = 0; x < shape[y].length; x++) {
                if (shape[y][x] > 0) {
                    this.updateBoard(shape[y][x], y + shapeY, x + shapeX);
                }
            }
        }
        
        const rowsComplete: number[] = [];
        for (let y = 0; y < this.board.length; y++) {
            if (this.isRowFull(y))
            {
                rowsComplete.push(y);
                this.board.splice(y, 1);
                this.board.unshift(new Array(this.board[0].length).fill(0));
                this.isDirty = true;
            }
        }
        return rowsComplete;
    }

    public getBoard(): number[][] {
        return this.board;
    }

    private isRowFull(i: number): boolean {
        return this.board[i].every((e) => { return e > 0; });
    }

    private isCollision(shape: number[][], shapeX: number, shapeY: number): boolean {
        for (let y = shape.length - 1; y >= 0; y--) {
            for (let x = 0; x < shape[y].length; x++) {
                if (shape[y][x] > 0) {
                    if (shapeY + y >= this.board.length || shapeY + y < - this.softHeight) {
                        return true;
                    }
                    if (shapeY + y >= 0)
                    {
                        if (this.board[y + shapeY][x + shapeX] > 0) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    private isInBounds(shape: number[][], shapeX: number, shapeY: number): boolean {
        for (let y = 0; y < shape.length; y++) {
            for (let x = 0; x < shape[y].length; x++) {
                if (shape[y][x] > 0) {
                    if (shapeY + y >= this.board.length || shapeY + y < -this.softHeight) {
                        return false;
                    }
                    if (shapeX + x >= this.board[0].length || shapeX + x < 0) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    private updateBoard(value: number, x: number, y: number): void {
        this.board[x][y] = value;
        this.isDirty = true;
    }

}