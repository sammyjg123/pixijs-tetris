import { ShapeDefinition } from "./shape-definition";

export class Shape {

    public shapeDefinition: ShapeDefinition;
    public grid: number[][];
    
    public locationX: number;
    public locationY: number;
    
    public isDirty: boolean;
    public didRotate: boolean;

    constructor(definition: ShapeDefinition, x: number = 0, y: number = 0) {
        this.shapeDefinition = definition;
        this.grid = definition.defaultGrid;
        this.locationX = x;
        this.locationY = y;
        this.isDirty = true;
        this.didRotate = false;
    }
      
    public rotateClockwise(): void {
        for (let y = 0; y < this.grid.length; y++) {
            for (let x = 0; x < y; x++) {
                let temp = this.grid[y][x];
                this.grid[y][x] = this.grid[x][y];
                this.grid[x][y] = temp;
            }
        }
        this.normaliseRotation();

        this.isDirty = true;
        this.didRotate = true;
    }
      
    public rotateCounterClockwise(): void {
        this.grid = this.grid.map((row) => {
            return row.reverse();
        });

        for (let y = 0; y < this.grid.length; y++) {
            for (let x = 0; x < y; x++) {
                let temp = this.grid[y][x];
                this.grid[y][x] = this.grid[x][y];
                this.grid[x][y] = temp;
            }
        }
        this.normaliseRotation();

        this.isDirty = true;
        this.didRotate = true;
    };

    public isRowEmpty(i: number): boolean {
        return this.grid[i].every((e) => { return e === 0; });
    };

    public isColumnEmpty(i: number): boolean {
        return this.grid.every((e) => { return e[i] === 0; });
    };

    private normaliseRotation(): void {
        for (let i = 0; i < this.shapeDefinition.rotationOffset; i++) {
            if (!this.isRowEmpty(0)) {
                let temp: number[] = this.grid.pop() as number[];
                this.grid.unshift(temp);
            }
            if (!this.isColumnEmpty(0)) {
                this.grid.forEach(row => {
                    let temp: number = row.pop() as number;
                    row.unshift(temp)
                });
            }
        }
    };
}