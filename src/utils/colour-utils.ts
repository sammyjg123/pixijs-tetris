import { NumericDictionary } from "lodash";
import { Texture } from "pixi.js";

export interface RGBColour {
    r: number;
    b: number;
    g: number;
}

export const Hex2RGB = (colour: number): RGBColour | null => {
    const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(colour.toString(16));
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;
}

export const vbColourToHex = (colour: number): string => {
    const subResult = ("000000" + colour.toString(16)).slice(-6);
    const result = subResult.substr(4, 2) + subResult.substr(2, 2) + subResult.substr(0, 2);
    return "#" + result;
}

export const gradient = (colours: number[]): Texture => {
    const result = document.createElement("canvas");
    const ctx = result.getContext("2d") as CanvasRenderingContext2D;
    const grd = ctx.createLinearGradient(0, 0, 100, 100);
    for (let i = 0; i < colours.length; i++) {
        grd.addColorStop(i / (colours.length - 1), vbColourToHex(colours[i]));
    }
    ctx.fillStyle = grd;
    ctx.fillRect(0, 0, 100, 100);
    return Texture.from(result);
}