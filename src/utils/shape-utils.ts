import { randomInt } from "./math-utils";
import { Shape } from "../logic/shape";
import { ShapeDefinition } from "../logic/shape-definition";

export class ShapeUtils {

    private shapeDefinitions: ShapeDefinition[];

    constructor() {
        this.shapeDefinitions = [
            new ShapeDefinition("I", 1, 0, [
                [0, 0, 1, 0],
                [0, 0, 1, 0],
                [0, 0, 1, 0],
                [0, 0, 1, 0]
            ]),
            new ShapeDefinition("O", 2, 0, [
                [0, 0, 0, 0],
                [0, 1, 1, 0],
                [0, 1, 1, 0],
                [0, 0, 0, 0]
            ]),
            new ShapeDefinition("T", 3, 1, [
                [0, 0, 0, 0],
                [0, 0, 1, 0],
                [0, 1, 1, 1],
                [0, 0, 0, 0]
            ]),
            new ShapeDefinition("S", 4, 1, [
                [0, 0, 0, 0],
                [0, 0, 1, 1],
                [0, 1, 1, 0],
                [0, 0, 0, 0]
            ]),
            new ShapeDefinition("Z", 5, 1, [
                [0, 0, 0, 0],
                [0, 1, 1, 0],
                [0, 0, 1, 1],
                [0, 0, 0, 0]
            ]),
            new ShapeDefinition("J", 6, 1, [
                [0, 0, 0, 0],
                [0, 1, 0, 0],
                [0, 1, 1, 1],
                [0, 0, 0, 0]
            ]),
            new ShapeDefinition("L", 7, 1, [
                [0, 0, 0, 0],
                [0, 0, 0, 1],
                [0, 1, 1, 1],
                [0, 0, 0, 0]
            ])
        ];

    };

    public getNewRandomShape(x: number = 0, y: number = 0) : Shape {
        const shapeID = randomInt(this.shapeDefinitions.length);
        return new Shape(this.shapeDefinitions[shapeID], x, y);
    }
}