import { Board } from "./logic/board";
import { ShapeUtils } from "./utils/shape-utils";
import { Shape } from "./logic/shape";
import { BoardVisuals } from "./visuals/board-visuals";
import { cloneDeep } from "lodash";
import { GameScreen } from "./visuals/game-screen";

export class Game {

    private gameVisuals: GameScreen;
    private boardVisuals: BoardVisuals;

    public gameBoard: Board;
    private shapeHandler: ShapeUtils;

    private currentShape: Shape;
    private nextShape: Shape;
    private score: number;
    private baseDropSpeed: number;
    private lastFrameTime: number;
    private lastBlockDropTime: number;

    private gameActive: boolean;
    private isDirty: boolean;

    static readonly boardWidth: number = 10;
    static readonly boardHeight: number = 20;
    static readonly softHeight: number = 3;

    constructor() {
        this.gameVisuals = new GameScreen();
        this.boardVisuals = new BoardVisuals();

        this.shapeHandler = new ShapeUtils();
        this.score = 0;
        this.baseDropSpeed = 1;
        this.lastFrameTime = Date.now();
        this.lastBlockDropTime = Date.now();
        
        this.gameActive = false;
        this.isDirty = false;
        this.gameBoard = new Board(Game.boardWidth, Game.boardHeight, Game.softHeight);
        
        this.currentShape = this.shapeHandler.getNewRandomShape(0, -Game.softHeight);
        this.nextShape = this.shapeHandler.getNewRandomShape(0, -Game.softHeight);

        this.gameLoop();

        document.addEventListener('keydown', (e) => {
            if (this.gameActive) {
                let canMove = false;
                switch(e.key) {
                    case "ArrowLeft":
                        canMove = this.gameBoard.tryMove(
                            this.currentShape.grid,
                            this.currentShape.locationX - 1,
                            this.currentShape.locationY
                        );
                        if (canMove) this.currentShape.locationX -= 1;
                        break;
                    case "ArrowRight":
                        canMove = this.gameBoard.tryMove(
                            this.currentShape.grid,
                            this.currentShape.locationX + 1,
                            this.currentShape.locationY
                        );
                        if (canMove) this.currentShape.locationX += 1;
                        break;
                    case "ArrowUp":
                        let newRotation = cloneDeep(this.currentShape);
                        newRotation.rotateCounterClockwise();
                        if (this.gameBoard.tryMove(newRotation.grid, newRotation.locationX, newRotation.locationY)) {
                            this.currentShape.rotateCounterClockwise();
                        };
                        break;
                    case "ArrowDown":
                        this.doBlockDrop();
                        break;
                    default: break; 
                }
            }
        });
        this.startGame();
    }

    public resetGame() {
        this.gameBoard = new Board(Game.boardWidth, Game.boardHeight, Game.softHeight);
        this.gameBoard.isDirty = true;
        this.isDirty = true;
        this.score = 0;
        this.lastBlockDropTime = Date.now();
    }

    public startGame(): void {
        this.resetGame();
        this.gameActive = true;
    }

    public endGame(): void {
        this.gameActive = false;

        // TODO: remove this once menus are in place
        this.startGame();
    }

    private gameLoop(): void {
        this.lastFrameTime = Date.now();

        if (this.gameActive) {
            if (this.isDirty) {
                this.currentShape = cloneDeep(this.nextShape);
                this.nextShape = this.shapeHandler.getNewRandomShape(0, -Game.softHeight);
                this.isDirty = false;

                if (!this.gameBoard.tryMove(this.currentShape.grid, this.currentShape.locationX, this.currentShape.locationY)) {
                    this.endGame();
                }
            }

            const timeSinceDrop = this.lastFrameTime - this.lastBlockDropTime;
            if (timeSinceDrop >= this.getDropSpeed()) {
                this.doBlockDrop();
            }
        } else {
            // TODO: check scores and allow menu to be shown
        }
        if (this.gameBoard.isDirty) this.boardVisuals.updateCurrentBoard(this.gameBoard);
        this.boardVisuals.updateCurrentShape(this.currentShape, Game.softHeight);
        this.gameVisuals.render();

        requestAnimationFrame(() => this.gameLoop())
    }

    private doBlockDrop(): void {
        const isValid = this.gameBoard.tryMove(
            this.currentShape.grid,
            this.currentShape.locationX,
            this.currentShape.locationY + 1
        );
        if (isValid) {
            this.currentShape.locationY += 1;
            this.lastBlockDropTime = Date.now()
            this.currentShape.isDirty = true;
            this.currentShape.didRotate = true;
        } else {
            if (this.currentShape.locationY > 0) {
                const removedRows = this.gameBoard.setPosition(
                    this.currentShape.grid,
                    this.currentShape.locationX,
                    this.currentShape.locationY
                );
                if (removedRows.length > 0) {
                    // Do some fancy scoring stuff
                }
                this.isDirty = true;
            } else {
                this.endGame();
            }
            this.lastBlockDropTime = this.lastFrameTime;
        }
    }

    private getDropSpeed(): number {
        const rawSpeedFactor = Math.pow(this.score / 1000, 2);
        const maxSpeedFactor = 0.9;
        const dropSpeed =  this.baseDropSpeed - Math.min(Math.max(rawSpeedFactor, 0), maxSpeedFactor);
        return dropSpeed * 1000;
    }
}